import java.util.Scanner;

public class DrawSquare {

	private static Scanner input;

	public static void main(String[] args)
	{
		input = new Scanner(System.in);
		int size;
		int countLength = 1;
		int countHeight = 1;
		
		System.out.print("Please enter the size of your square:");
		size = input.nextInt();
		
		if (size>0)	
		{	
			//after one line finishes repeats until number of lines is the same as size
			while(countHeight<=size)
			{	
				// while loop writes * for as long as the length of the square
				while(countLength<=size)
				{
					System.out.print("*");
					countLength++;
				} 
				
				System.out.print("\n");
				countHeight++;
				countLength = 1;
			}//end first while 
		}// end if
		
		
		
		

	}//end method

}
